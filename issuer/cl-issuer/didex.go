package main

import (
	"fmt"
	"github.com/hyperledger/aries-framework-go/pkg/client/didexchange"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/common/service"
	didexservice "github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/didexchange"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	"github.com/sirupsen/logrus"
)

func AddDidExchange(ctx *context.Provider) error {
	logrus.Infof("adding did exchange support to the agent")

	didExActions := make(chan service.DIDCommAction, 10)
	didExchangeClient, err := didexchange.New(ctx)
	if err != nil {
		return fmt.Errorf("failed to create did exchange client, reason: %w", err)
	}

	err = didExchangeClient.RegisterActionEvent(didExActions)

	if err != nil {
		return fmt.Errorf("failed to register did exchange actions, reason: %w", err)
	}

	// accept all didEx actions
	go func() {
		for action := range didExActions {
			go handleDidExActions(didExchangeClient, action)
		}
	}()

	logrus.Infof("successfully added did exchange support to the agent")

	return nil
}

func handleDidExActions(client *didexchange.Client, action service.DIDCommAction) {
	var err error

	if action.Message.Type() == didexservice.RequestMsgType {
		err = handleDidExRequest(client, action)
	}
	if err != nil {
		logrus.Errorf("failed to handle a did exchange action, %s", err)
	}
}

func handleDidExRequest(client *didexchange.Client, action service.DIDCommAction) error {
	if isConnectionAllowed(&action.Message) {
		prop := action.Properties.(didexchange.Event)
		err := client.AcceptExchangeRequest(prop.ConnectionID(), "", "test")
		if err != nil {
			return fmt.Errorf("couldn't accept exchange request, reason: %s", err)
		}
	}

	return nil
}

func isConnectionAllowed(_message *service.DIDCommMsg) bool {
	// we accept all connections for now
	return true
}
