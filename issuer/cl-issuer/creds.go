package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	client "github.com/hyperledger/aries-framework-go/pkg/client/issuecredential"
	"github.com/hyperledger/aries-framework-go/pkg/client/issuecredential/rfc0593"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/common/service"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/decorator"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/issuecredential"
	"github.com/hyperledger/aries-framework-go/pkg/doc/util"
	"github.com/hyperledger/aries-framework-go/pkg/doc/verifiable"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	credx "github.com/hyperledger/indy-shared-rs/wrappers/golang/indy-credx/types"
	"github.com/sirupsen/logrus"
	"time"
)

func AddCreds(ctx *context.Provider) error {
	logrus.Infof("adding credential issuing support to the agent")

	issueCredActions := make(chan service.DIDCommAction, 10)
	issueCredentialSvc, err := client.New(ctx)
	if err != nil {
		return fmt.Errorf("failed to create issue credential client, reason: %w", err)
	}
	err = issueCredentialSvc.RegisterActionEvent(issueCredActions)
	if err != nil {
		return fmt.Errorf("failed to register issue credential actions, reason: %w", err)
	}

	go func() {
		for action := range issueCredActions {
			go handleIssueCredActions(ctx, issueCredentialSvc, action)
		}
	}()

	logrus.Infof("successfully added credential issuing support to the agent")

	return nil
}

func handleIssueCredActions(ctx *context.Provider, service *client.Client, action service.DIDCommAction) {
	var err error

	switch action.Message.Type() {
	case issuecredential.ProposeCredentialMsgTypeV2:
		err = handleProposeCredential(ctx, service, action)
	case issuecredential.RequestCredentialMsgTypeV2:
		err = handleRequestCredential(ctx, service, action)
	}

	if err != nil {
		logrus.Errorf("failed to handle an issue cred action, %s", err)
	}
}

func handleProposeCredential(ctx *context.Provider, service *client.Client, action service.DIDCommAction) error {
	proposal := &client.ProposeCredentialV2{}
	err := action.Message.Decode(proposal)

	if err != nil {
		return fmt.Errorf("couldn't decode the proposal, reason %w", err)
	}
	piid, ok := action.Properties.All()["piid"].(string)

	logrus.WithField("piid", piid).Infof("handling credential proposal")
	logrus.WithFields(logrus.Fields{
		"piid": piid,
	}).Debugf("received credential proposal")

	if !ok {
		return fmt.Errorf("couldn't get piid from propsal")
	}

	offerCredential, err := getOfferCredentialMsg(ctx, piid)
	err = service.AcceptProposal(piid, offerCredential)

	logrus.WithField("piid", piid).Infof("sending credential offer")
	logrus.WithFields(logrus.Fields{
		"piid":            piid,
		"offerCredential": offerCredential,
	}).Debugf("proposal was accepted, responding with credential offer")

	if err != nil {
		return fmt.Errorf("couldn't accept the proposal, reason %w", err)
	}

	return nil
}

func getOfferCredentialMsg(ctx *context.Provider, piID string) (*issuecredential.OfferCredentialParams, error) {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return nil, fmt.Errorf("failed to open credx store, reason %s", err)
	}

	schemaJSON, err := store.Get(schemaID)
	if err != nil {
		return nil, fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	schema, err := credx.LoadSchemaFromJSON(schemaJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	schemaID, err := schema.GetID()
	if err != nil {
		return nil, fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	credDefJSON, err := store.Get(credDefID)
	if err != nil {
		return nil, fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	keyCorrectnessJSON, err := store.Get(keyCorrectnessID)
	if err != nil {
		return nil, fmt.Errorf("failed to load keyCorrectness json, reason %s", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON(credDefJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to load credDef from json, reason %s", err)
	}

	proof, err := credx.LoadKeyCorrectnessProofFromJSON(keyCorrectnessJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to load keyCorrectnessProof from json, reason %s", err)
	}

	offer, err := credx.NewCredentialOffer(schemaID, credDef, proof)
	if err != nil {
		return nil, fmt.Errorf("failed to create credential offer, reason %s", err)
	}

	credOffer, err := offer.ToJSON()
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credential offer, reason %s", err)
	}

	fmt.Println(credxStateStorePrefix + "offer_" + piID)
	err = store.Put(credxStateStorePrefix+"offer_"+piID, credOffer)
	if err != nil {
		return nil, fmt.Errorf("couldn't save offer to credx store")
	}

	attachID := uuid.New().String()
	return &client.OfferCredential{
		Type: issuecredential.OfferCredentialMsgTypeV2,
		Formats: []issuecredential.Format{{
			AttachID: attachID,
			Format:   rfc0593.ProofVCFormat,
		}},
		Attachments: []decorator.GenericAttachment{{
			ID: attachID,
			Data: decorator.AttachmentData{
				JSON: credOffer,
			},
		}},
	}, nil
}

func handleRequestCredential(ctx *context.Provider, service *client.Client, action service.DIDCommAction) error {
	credReq := &client.RequestCredentialV2{}
	err := action.Message.Decode(credReq)
	if err != nil {
		return fmt.Errorf("couldn't decode credReq, reason %s", err)
	}

	piid, ok := action.Properties.All()["piid"].(string)
	if !ok {
		return fmt.Errorf("couldn't get piid")
	}

	logrus.WithField(
		"piid",
		piid,
	).Infof("received credential request")

	credential, err := getIssueCredentialMsg(
		ctx,
		piid,
		credReq,
	)
	if err != nil {
		return fmt.Errorf("couldn't create credential, reason: %w", err)
	}

	err = service.AcceptRequest(piid, credential)

	logrus.WithFields(logrus.Fields{
		"credential": credential,
		"piid":       piid,
	}).Debugf("accepted credential request (issuing the credential)")

	if err != nil {
		return fmt.Errorf("couldn't accept request, reason: %w", err)
	}

	return nil
}

func getIssueCredentialMsg(ctx *context.Provider, piID string, credReq *client.RequestCredentialV2) (*issuecredential.IssueCredentialParams, error) {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	credDefJSON, err := store.Get(credDefID)
	if err != nil {
		return nil, fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON(credDefJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to load credDef from json, reason %s", err)
	}

	credDefPvtJSON, err := store.Get(credDefPvtID)
	if err != nil {
		return nil, fmt.Errorf("failed to load creddefpvt json, reason %s", err)
	}

	credDefPvt, err := credx.LoadCredentialDefinitionPrivateFromJSON(credDefPvtJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to load credDefPvt from json, reason %s", err)
	}

	credReqJSON, err := json.Marshal(credReq.RequestsAttach[0].Data.JSON)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credReq, reason %s", err)
	}

	fmt.Println(credxStateStorePrefix + "offer_" + piID)
	offerJSON, err := store.Get(credxStateStorePrefix + "offer_" + piID)
	if err != nil {
		return nil, fmt.Errorf("failed to load credx offer from store, reason %s", err)
	}

	credxOffer, err := credx.LoadCredentialOfferFromJSON(offerJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to load credx offer from json, reason %s", err)
	}

	credxCredReq, err := credx.LoadCredentialRequestFromJSON(credReqJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credReq, reason %s", err)
	}

	revRegDefJSON, err := store.Get(revRegDefID)
	if err != nil {
		return nil, fmt.Errorf("failed to load rev reg def, reason %w", err)
	}

	revRegDefPvtJSON, err := store.Get(revRegDefPvtID)
	if err != nil {
		return nil, fmt.Errorf("failed to load rev reg def pvt, reason %w", err)
	}

	revRegJSON, err := store.Get(revRegID)
	if err != nil {
		return nil, fmt.Errorf("failed to load rev reg, reason %w", err)
	}

	revRegDef, err := credx.LoadRevocationRegistryDefinitionFromJSON(revRegDefJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshall rev reg def, reason %w", err)
	}

	revRegDefPvt, err := credx.LoadRevocationRegistryDefinitionPrivateFromJSON(revRegDefPvtJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshall rev reg def pvt, reason %w", err)

	}

	revReg, err := credx.LoadRevocationRegistryFromJSON(revRegJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to unmarshall rev reg, reason %w", err)
	}

	tails, err := revRegDef.GetTailsLocation()
	if err != nil {
		return nil, fmt.Errorf("couldn't get tails ID from rev reg def, reason %w", err)
	}

	cred, _, _, err := credx.NewCredential(
		credDef,
		credDefPvt,
		credxOffer,
		credxCredReq,
		credx.NewCredentialRevocationInfo(
			revRegDef,
			revRegDefPvt,
			revReg,
			1,
			[]int64{},
			tails,
		),
		[]string{"testattr"},
		[]string{"testvalue"},
		[]string{},
	)
	if err != nil {
		return nil, fmt.Errorf("couldn't create credential, reason %s", err)
	}

	credJSON, err := cred.ToJSON()
	if err != nil {
		return nil, fmt.Errorf("couldn't marshall credential, reason %s", err)
	}

	var data map[string]interface{}
	err = json.Unmarshal(credJSON, &data)
	if err != nil {
		return nil, fmt.Errorf("couldn't conver credential to map[string]interface{}, reason %s", err)
	}

	vc, err := getVC(data)
	if err != nil {
		return nil, err
	}

	attachID := uuid.New().String()
	msg := &issuecredential.IssueCredentialParams{
		Type: issuecredential.IssueCredentialMsgTypeV2,
		Formats: []issuecredential.Format{{
			AttachID: attachID,
			Format:   rfc0593.ProofVCFormat,
		}},
		Attachments: []decorator.GenericAttachment{{
			ID: attachID,
			Data: decorator.AttachmentData{
				JSON: vc,
			},
		}},
	}

	return msg, nil
}

func getVC(data map[string]interface{}) (*verifiable.Credential, error) {
	issuedTime := time.Now()
	vc := &verifiable.Credential{
		Context: []string{verifiable.ContextURI},
		Types:   []string{"eid"},
		ID:      uuid.New().URN(),
		Subject: verifiable.Subject{
			ID:           uuid.New().URN(),
			CustomFields: data,
		},
		Issuer: verifiable.Issuer{
			ID: issuerDID,
		},

		Issued:  util.NewTime(issuedTime),
		Expired: util.NewTime(issuedTime.Add(time.Duration(24*180) * time.Hour)),
	}

	return vc, nil
}
