package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/tink/go/subtle/random"
	"github.com/google/uuid"
	client "github.com/hyperledger/aries-framework-go/pkg/client/presentproof"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/common/service"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/decorator"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/presentproof"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	credx "github.com/hyperledger/indy-shared-rs/wrappers/golang/indy-credx/types"
	"github.com/sirupsen/logrus"
	"strconv"
	"time"
)

func AddPres(ctx *context.Provider) error {
	logrus.Infof("adding credential issuing support to the agent")

	presActions := make(chan service.DIDCommAction, 10)
	presentProofClient, err := client.New(ctx)
	if err != nil {
		return fmt.Errorf("failed to create issue credential client, reason: %w", err)
	}
	err = presentProofClient.RegisterActionEvent(presActions)
	if err != nil {
		return fmt.Errorf("failed to register issue credential actions, reason: %w", err)
	}

	go func() {
		for action := range presActions {
			go handlePresentProofActions(ctx, presentProofClient, action)
		}
	}()

	logrus.Infof("successfully added credential issuing support to the agent")

	return nil
}

func handlePresentProofActions(ctx *context.Provider, presentProofClient *client.Client, action service.DIDCommAction) {
	var err error

	switch action.Message.Type() {
	case presentproof.ProposePresentationMsgTypeV2:
		err = handleProposePresentation(ctx, presentProofClient, action)
	case presentproof.PresentationMsgTypeV2:
		err = handlePresentation(ctx, presentProofClient, action)
	}

	if err != nil {
		logrus.Errorf("failed to handle an issue cred action, %s", err)
	}
}

func handleProposePresentation(ctx *context.Provider, proofClient *client.Client, action service.DIDCommAction) error {
	proposal := &client.ProposePresentationV2{}
	err := action.Message.Decode(proposal)
	if err != nil {
		return fmt.Errorf("couldn't decode the proposal, reason %w", err)
	}

	piid, ok := action.Properties.All()["piid"].(string)
	if !ok {
		return fmt.Errorf("couldn't get piID from presentation req")
	}

	logrus.WithField(
		"piid",
		piid,
	).Infof("presentation proposal received")

	presReq, err := newPresentationRequest(ctx, piid)
	if err != nil {
		return fmt.Errorf("failed to create credx pres req, reason %s", err)
	}

	attachID := uuid.New().String()
	err = proofClient.AcceptProposePresentation(
		piid,
		&presentproof.RequestPresentationParams{
			Comment:     "credx presentation",
			WillConfirm: false,
			Attachments: []decorator.GenericAttachment{{
				ID: attachID,
				Data: decorator.AttachmentData{
					JSON: presReq,
				},
			}},
		},
	)

	if err != nil {
		return fmt.Errorf("couldn't accept presentation proposal, reason %s", err)
	}

	logrus.WithFields(logrus.Fields{
		"piid":                piid,
		"presentationRequest": presReq,
	}).Infof("presentation request was sent")

	return nil
}

func newPresentationRequest(ctx *context.Provider, piID string) (map[string]interface{}, error) {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return nil, fmt.Errorf("couldn't open credx store, reason %s", err)
	}
	referent := "reft"
	nonce := random.GetRandomUint32()
	now := time.Now()
	reqJSON := []byte(
		fmt.Sprintf(
			`{"name":"proof","version":"1.0","nonce":"%d","requested_attributes":{"%s":{"name":"testattr","non_revoked":{"from":%d,"to":%d}}},"requested_predicates":{},"ver":"1.0"}`,
			nonce,
			referent,
			now.Unix(),
			now.Unix(),
		),
	)

	_, err = credx.PresentationRequestFromJSON(reqJSON)
	if err != nil {
		return nil, fmt.Errorf("couldn't create presentation req from json, reason: %s", err)
	}

	var req map[string]interface{}
	err = json.Unmarshal(reqJSON, &req)
	if err != nil {
		return nil, fmt.Errorf("couldn't unmarshall cred req to map[string]interface{}, reason: %s", err)
	}

	err = store.Put(credxStateStorePrefix+"presentation_"+piID, reqJSON)
	if err != nil {
		return nil, fmt.Errorf("couldn't store cred req to store, reason: %s", err)
	}

	return req, nil
}

func handlePresentation(ctx *context.Provider, _proofClient *client.Client, action service.DIDCommAction) error {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return fmt.Errorf("couldn't open credx store, reason %s", err)
	}

	presentation := &client.PresentationV2{}
	err = action.Message.Decode(presentation)
	if err != nil {
		return fmt.Errorf("couldn't decode the presentation, reason %w", err)
	}

	piid, ok := action.Properties.All()["piid"].(string)
	if !ok {
		return fmt.Errorf("couldn't get piID from presentation")
	}
	logrus.WithField(
		"piid",
		piid,
	).Infof("presentation received")

	presJSON, err := json.Marshal(presentation.PresentationsAttach[0].Data.JSON)
	if err != nil {
		return fmt.Errorf("couldn't marshall presentation to json, reason %s", err)
	}

	credxPresentation, err := credx.LoadPresentationFromJSON(presJSON)
	if err != nil {
		return fmt.Errorf("couldn't load presentation from json, reason %s", err)
	}

	presReqJSON, err := store.Get(credxStateStorePrefix + "presentation_" + piid)
	if err != nil {
		return fmt.Errorf("couldn't load presentation req from store, reason %s", err)
	}

	presReq, err := credx.PresentationRequestFromJSON(presReqJSON)
	if err != nil {
		return fmt.Errorf("couldn't load pres req from json, reason %s", err)
	}

	var presReqMap map[string]interface{}
	err = json.Unmarshal(presReqJSON, &presReqMap)
	if err != nil {
		return fmt.Errorf("couldn't unmarshall pres req, reason %w", err)
	}

	timestamp := presReqMap["requested_attributes"].(map[string]interface{})["reft"].(map[string]interface{})["non_revoked"].(map[string]interface{})["from"]

	schemaJSON, err := store.Get(schemaID)
	if err != nil {
		return fmt.Errorf("failed to load schema json, reason %s", err)
	}

	schema, err := credx.LoadSchemaFromJSON(schemaJSON)
	if err != nil {
		return fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	credDefJSON, err := store.Get(credDefID)
	if err != nil {
		return fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON(credDefJSON)
	if err != nil {
		return fmt.Errorf("failed to load credDef from json, reason %s", err)
	}

	revRegDefJSON, err := store.Get(revRegDefID)
	if err != nil {
		return fmt.Errorf("failed to load rev reg def, reason %w", err)
	}

	revRegDef, err := credx.LoadRevocationRegistryDefinitionFromJSON(revRegDefJSON)
	if err != nil {
		return fmt.Errorf("failed to unmarshall rev reg def, reason %w", err)
	}

	revRegJSON, err := store.Get(revRegID)
	if err != nil {
		return fmt.Errorf("failed to load rev reg, reason %w", err)
	}

	revReg, err := credx.LoadRevocationRegistryFromJSON(revRegJSON)
	if err != nil {
		return fmt.Errorf("failed to unmarshall rev reg, reason %w", err)
	}

	timestampInt, _ := strconv.ParseInt(fmt.Sprint(timestamp), 10, 64)
	status, err := credxPresentation.Verify(
		presReq,
		[]credx.Schema{*schema},
		[]credx.CredentialDefinition{*credDef},
		[]credx.RevocationRegistryDefinition{*revRegDef},
		[]credx.RevocationEntry{
			*credx.NewRevocationEntry(0, revReg, timestampInt),
		},
	)
	if err != nil {
		return fmt.Errorf("couldn't verify presentation, reason %s", err)
	}

	if status != true {
		return fmt.Errorf("credential verification status was false")
	}

	logrus.WithField(
		"piid",
		piid,
	).Infof("successfully verified presentation")

	return nil
}
