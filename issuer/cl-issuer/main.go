package main

import (
	"crypto/tls"
	"flag"
	"fmt"
	"github.com/google/uuid"
	"github.com/hyperledger/aries-framework-go/component/storage/leveldb"
	"github.com/hyperledger/aries-framework-go/pkg/client/didexchange"
	arieshttp "github.com/hyperledger/aries-framework-go/pkg/didcomm/transport/http"
	"github.com/hyperledger/aries-framework-go/pkg/framework/aries"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	credx "github.com/hyperledger/indy-shared-rs/wrappers/golang/indy-credx/types"
	"github.com/kataras/iris/v12"
	"github.com/sirupsen/logrus"
	"io"
	"log"
	"net/http"
	"os"
	"strconv"
)

const (
	credxStoreName        = "credx"
	credxStateStorePrefix = "credx_state_"
	issuerDID             = "55GkHamhTU1ZbTbV2ab9DE"
	schemaID              = "schema1"
	schemaName            = "testSchema"
	schemaVersion         = "1"

	credDefID        = "credDef1"
	credDefPvtID     = "credDefPvt1"
	keyCorrectnessID = "keyCor1"

	revRegDefID    = "revRegDef1"
	revRegDefPvtID = "revRegPvt1"
	revRegID       = "revReg1"
	revRegDeltaID  = "revRegDelta1"
)

var (
	logLevel = flag.String(
		"log-level",
		"debug",
		"Possible values: 'trace', 'debug', 'info', 'warning', 'error', 'fatal', 'panic"+
			"Can be set with env var 'LOG_LEVEL'",
	)
	controllerHostPort = flag.String(
		"controller-port",
		":8080",
		"This port is used to start the http server that acts as the invitation endpoint."+
			"Can be set with env var 'CONTROLLER_PORT'",
	)
	ariesInternalListenAddress = flag.String(
		"http-inbound-host",
		"0.0.0.0:6789",
		"Aries Inbound Host Name:Port. This is used internally to start the inbound aries server."+
			"Can be set with env var 'HTTP_INBOUND_HOST'",
	)
	ariesExternalListenAddress = flag.String(
		"http-host-external",
		"http://localhost:6789",
		"Aries Inbound Host Name:Port. This is the URL for the inbound server as seen externally."+
			"Can be set with env var 'HTTP_HOST_EXTERNAL'",
	)
	credxVDREndpoint = flag.String(
		"credx-vdr-endpoint",
		"http://localhost:9999",
		"credx verifiable data registry endpoint. Used only for testing."+
			"Can be set with env var 'CREDX_VDR_ENDPOINT'",
	)
	allowInsecureHttps = flag.Bool(
		"allow-insecure-tls",
		true,
		"Whether to allow insecure TLS connections. Should be true only when testing."+
			"Can be set with env var 'ALLOW_INSECURE_TLS'",
	)
)

// loadSettings load the settings from flags and env vars. Env vars have priority over flags
func loadSettings() {
	flag.Parse()
	if val := os.Getenv("LOG_LEVEL"); val != "" {
		logLevel = &val
	}
	if val := os.Getenv("HTTP_INBOUND_HOST"); val != "" {
		ariesInternalListenAddress = &val
	}
	if val := os.Getenv("HTTP_HOST_EXTERNAL"); val != "" {
		ariesExternalListenAddress = &val
	}
	if val := os.Getenv("CONTROLLER_PORT"); val != "" {
		controllerHostPort = &val
	}
	if val := os.Getenv("CREDX_VDR_ENDPOINT"); val != "" {
		credxVDREndpoint = &val
	}
	if val := os.Getenv("ALLOW_INSECURE_TLS"); val != "" {
		readVal, _ := strconv.ParseBool(val)
		allowInsecureHttps = &readVal
	}
}

func main() {
	loadSettings()
	err := setUpGlobalParams()
	if err != nil {
		log.Fatal(err)
	}

	ctx, err := startAriesAgent()
	if err != nil {
		log.Fatal(err)
	}

	didexClient, err := didexchange.New(ctx)
	if err != nil {
		log.Fatal(err)
	}

	err = startControllerHttpServer(didexClient)
	if err != nil {
		log.Fatal(err)
	}
}

func startControllerHttpServer(
	didExchangeClient *didexchange.Client,
) error {
	app := iris.New()

	app.Get("/health", func(ctx iris.Context) {
		ctx.StatusCode(200)
	})
	app.Get("/invitation", func(ctx iris.Context) {
		label, err := uuid.NewUUID()
		if err != nil {
			ctx.StatusCode(500)
			return
		}

		invitation, err := didExchangeClient.CreateInvitation(label.String())
		if err != nil {
			ctx.StatusCode(500)
			return
		}

		_, err = ctx.JSON(invitation)
		if err != nil {
			ctx.StatusCode(500)
			return
		}
	})
	err := app.Listen(*controllerHostPort)

	return err
}

type HttpClient interface {
	Get(url string) (*http.Response, error)
	Post(url, contentType string, body io.Reader) (*http.Response, error)
}

type basicAppCtx struct {
	http          HttpClient
	cache         map[string]string
	credxVDR      credxVDR
	ariesProvider *context.Provider
}

var defaultCtx *basicAppCtx

func AppCtx() *basicAppCtx {
	if defaultCtx != nil {
		return defaultCtx
	}

	defaultCtx = &basicAppCtx{
		http:  http.DefaultClient,
		cache: make(map[string]string),
		credxVDR: &simpleRestCredxVDR{
			httpClient: http.DefaultClient,
			endpoint:   *credxVDREndpoint,
		},
	}

	return defaultCtx
}

// setUpGlobalParams sets up the parameters used in the app.
func setUpGlobalParams() error {
	level, err := logrus.ParseLevel(*logLevel)
	if err != nil {
		return fmt.Errorf("failed to setup logrus, reason %w", err)
	}
	logrus.SetLevel(level)
	// TODO: don't allow insecure TLS connections on prod
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: *allowInsecureHttps}
	if *allowInsecureHttps {
		logrus.Warn("insecure TLS connections are allowed, this shouldn't be the case on prod systems")
	}

	return nil
}

func startAriesAgent() (*context.Provider, error) {
	// TLS certs left empty on purpose
	inboundHttp, err := arieshttp.NewInbound(
		*ariesInternalListenAddress, *ariesExternalListenAddress, "", "",
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create inbound transport method, reason: %w", err)
	}

	framework, err := aries.New(
		aries.WithInboundTransport(inboundHttp),
		aries.WithStoreProvider(
			leveldb.NewProvider("testdb"),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create framework, reason: %w", err)
	}

	// get the context from the framework
	ctx, err := framework.Context()
	if err != nil {
		return nil, fmt.Errorf("failed to create framework context, reason: %w", err)
	}

	err = AddDidExchange(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to add didExchangeClient to the framework context, reason: %w", err)
	}

	err = AddCreds(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to add issuecredential to the framework context, reason: %w", err)
	}

	err = AddPres(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to add presentproof to the framework context, reason: %w", err)
	}

	err = setupSchemas(ctx)
	if err != nil {
		return nil, fmt.Errorf("couldn't create cred schema, reason %w", err)
	}

	err = setupCredDef(ctx)
	if err != nil {
		return nil, fmt.Errorf("couldn't create cred def, reason %w", err)
	}
	err = setupRevReg(ctx)
	if err != nil {
		return nil, fmt.Errorf("couldn't create rev reg, reason %w", err)
	}

	logrus.WithFields(
		logrus.Fields{
			"ariesInternalListenAddress": *ariesInternalListenAddress,
			"ariesExternalListenAddress": *ariesExternalListenAddress,
		}).Infof("started aries agent")

	return ctx, err
}

func setupSchemas(ctx *context.Provider) error {
	schema, err := credx.NewSchema(
		issuerDID,
		schemaName,
		schemaVersion,
		[]string{
			"testattr",
		},
		int64(1),
	)
	if err != nil {
		return fmt.Errorf("failed to setup schema, reason %w", err)
	}

	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return fmt.Errorf("couldn't open storage provider, reason %w", err)
	}

	schemaJSON, err := schema.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't get schema as json, reason %w", err)
	}

	err = store.Put(schemaID, schemaJSON)
	if err != nil {
		return fmt.Errorf("couldn't store schema as json, reason %w", err)
	}

	schemaID, err := schema.GetID()
	if err != nil {
		return fmt.Errorf("couldn't get schemaID, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(schemaID, schemaJSON)
	if err != nil {
		return fmt.Errorf("couldn't store schema to VDR, reason %w", err)
	}

	return nil
}

func setupCredDef(ctx *context.Provider) error {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return fmt.Errorf("couldn't open storage provider, reason %w", err)
	}
	schemaJSON, err := store.Get(schemaID)
	if err != nil {
		return fmt.Errorf("couldn't open storage provider, reason %w", err)
	}

	schema, err := credx.LoadSchemaFromJSON(schemaJSON)
	if err != nil {
		return fmt.Errorf("couldn't load schema from JSON, reason %w", err)
	}

	credDef, credDefPvt, keyCorrectness, err := credx.NewCredentialDefinition(
		issuerDID,
		schema,
		"testtag",
		"CL",
		true,
	)
	if err != nil {
		return fmt.Errorf("couldn't create credential definition, reason %w", err)
	}

	credDefJSON, err := credDef.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall credDef, reason %w", err)
	}

	credDefPvtJSON, err := credDefPvt.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall credDefPvt, reason %w", err)
	}

	keyCorrectJSON, err := keyCorrectness.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall keyCorrectnessProof, reason %w", err)
	}

	err = store.Put(credDefID, credDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't store credDef, reason %w", err)
	}

	err = store.Put(credDefPvtID, credDefPvtJSON)
	if err != nil {
		return fmt.Errorf("couldn't store credDefPvt, reason %w", err)
	}

	err = store.Put(keyCorrectnessID, keyCorrectJSON)
	if err != nil {
		return fmt.Errorf("couldn't store keyCorrectness, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(credDefID, credDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't store cred def to VDR, reason %w", err)
	}

	credDefID, err := credDef.GetID()
	if err != nil {
		return fmt.Errorf("couldn't get cred def ID, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(credDefID, credDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't store cred def to VDR, reason %w", err)
	}

	return nil
}

func setupRevReg(ctx *context.Provider) error {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return fmt.Errorf("couldn't open storage provider, reason %w", err)
	}
	credDefJSON, err := store.Get(credDefID)
	if err != nil {
		return fmt.Errorf("couldn't open storage provider, reason %w", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON(
		credDefJSON,
	)
	if err != nil {
		return fmt.Errorf("couldn't create credential definition, reason %w", err)
	}

	revRegDef, revRegDefPrivate, revReg, revRegDelta, err := credx.NewRevocationRegistryDefinition(
		issuerDID,
		credDef,
		"default",
		"CL_ACCUM",
		"",
		int64(100),
		"/tmp/",
	)
	if err != nil {
		return fmt.Errorf("couldn't create a revocation registry definition, reason: %w", err)
	}

	revRegDefJSON, err := revRegDef.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall revocation registry definition, reason: %w", err)
	}

	revRegDefPrivateJSON, err := revRegDefPrivate.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall revocation registry pvt, reason: %w", err)
	}

	revRegJSON, err := revReg.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall revocation registry, reason: %w", err)
	}

	revRegDeltaJSON, err := revRegDelta.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall revocation registry delta, reason: %w", err)
	}

	err = store.Put(revRegDefID, revRegDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revRegDef, reason %w", err)
	}

	err = store.Put(revRegDefPvtID, revRegDefPrivateJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revRegDef, reason %w", err)
	}

	err = store.Put(revRegID, revRegJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revReg, reason %w", err)
	}

	err = store.Put(revRegDeltaID, revRegDeltaJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revReg delta, reason %w", err)
	}

	revRegDefID, err := revRegDef.GetID()
	if err != nil {
		return fmt.Errorf("couldn't get rev reg ID, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(revRegDefID, revRegDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revRegDef to VDR, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(revRegDefID+"entry", revRegJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revReg to VDR, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(revRegDefID+"delta", revRegDeltaJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revRegDelta to VDR, reason %w", err)
	}

	err = AppCtx().credxVDR.Put(revRegDeltaID, revRegDeltaJSON)
	if err != nil {
		return fmt.Errorf("couldn't store revRegDelta to VDR, reason %w", err)
	}

	return nil
}
