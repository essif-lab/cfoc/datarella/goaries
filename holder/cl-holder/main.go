package main

import (
	"crypto/tls"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/hyperledger/aries-framework-go/component/storage/leveldb"
	"github.com/hyperledger/aries-framework-go/pkg/client/didexchange"
	"github.com/hyperledger/aries-framework-go/pkg/client/issuecredential"
	"github.com/hyperledger/aries-framework-go/pkg/client/presentproof"
	arieshttp "github.com/hyperledger/aries-framework-go/pkg/didcomm/transport/http"
	"github.com/hyperledger/aries-framework-go/pkg/framework/aries"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	"io"
	"os"
	"strconv"
	"time"

	"github.com/sirupsen/logrus"
	"io/ioutil"
	"log"
	"net/http"
)

var (
	event              = make(chan bool)
	issuerConnectionID = ""
	credxStoreName     = "credx"
)

var (
	logLevel = flag.String(
		"log-level",
		"debug",
		"Possible values: 'trace', 'debug', 'info', 'warning', 'error', 'fatal', 'panic"+
			"Can be set with env var 'LOG_LEVEL'",
	)
	ariesInternalListenAddress = flag.String(
		"http-inbound-host",
		"0.0.0.0:7789",
		"Aries Inbound Host Name:Port. This is used internally to start the inbound aries server."+
			"Can be set with env var 'HTTP_INBOUND_HOST'",
	)
	ariesExternalListenAddress = flag.String(
		"http-host-external",
		"http://localhost:7789",
		"Aries Inbound Host Name:Port. This is the URL for the inbound server as seen externally."+
			"Can be set with env var 'HTTP_HOST_EXTERNAL'",
	)
	issuerInvitationEndpoint = flag.String(
		"issuer-invitation-endpoint",
		"http://localhost:8080/invitation",
		"Issuer's invitation endpoint."+
			"Can be set with env var 'ISSUER_INVITATION_ENDPOINT'",
	)
	credxVDREndpoint = flag.String(
		"credx-vdr-endpoint",
		"http://localhost:9999",
		"credx verifiable data registry endpoint. Used only for testing."+
			"Can be set with env var 'CREDX_VDR_ENDPOINT'",
	)
	allowInsecureHttps = flag.Bool(
		"allow-insecure-tls",
		true,
		"Whether to allow insecure TLS connections. Should be true only when testing."+
			"Can be set with env var 'ALLOW_INSECURE_TLS'",
	)
)

// loadSettings load the settings from flags and env vars. Env vars have priority over flags
func loadSettings() {
	flag.Parse()
	if val := os.Getenv("LOG_LEVEL"); val != "" {
		logLevel = &val
	}
	if val := os.Getenv("HTTP_INBOUND_HOST"); val != "" {
		ariesInternalListenAddress = &val
	}
	if val := os.Getenv("HTTP_HOST_EXTERNAL"); val != "" {
		ariesExternalListenAddress = &val
	}
	if val := os.Getenv("ISSUER_INVITATION_ENDPOINT"); val != "" {
		issuerInvitationEndpoint = &val
	}
	if val := os.Getenv("CREDX_VDR_ENDPOINT"); val != "" {
		credxVDREndpoint = &val
	}
	if val := os.Getenv("ALLOW_INSECURE_TLS"); val != "" {
		readVal, _ := strconv.ParseBool(val)
		allowInsecureHttps = &readVal
	}
}

func main() {
	loadSettings()
	err := setUpGlobalParams()
	if err != nil {
		log.Fatal(err)
	}

	ctx, err := startAriesAgent()
	if err != nil {
		log.Fatal(err)
	}

	err = connectToIssuer(ctx)
	if err != nil {
		log.Fatal(err)
	}
	time.Sleep(time.Second * 3)

	err = proposeCredential(ctx)
	if err != nil {
		log.Fatal(err)
	}

	<-event

	err = proposePresentation(ctx)
	if err != nil {
		log.Fatal(err)
	}

	select {}
}

type HttpClient interface {
	Get(url string) (*http.Response, error)
	Post(url, contentType string, body io.Reader) (*http.Response, error)
}

type basicAppCtx struct {
	http          HttpClient
	cache         map[string]string
	credxVDR      credxVDR
	ariesProvider *context.Provider
}

var defaultCtx *basicAppCtx

func AppCtx() *basicAppCtx {
	if defaultCtx != nil {
		return defaultCtx
	}

	defaultCtx = &basicAppCtx{
		http:  http.DefaultClient,
		cache: make(map[string]string),
		credxVDR: &simpleRestCredxVDR{
			httpClient: http.DefaultClient,
			endpoint:   *credxVDREndpoint,
		},
	}

	return defaultCtx
}

func connectToIssuer(ctx *context.Provider) error {
	client, err := didexchange.New(ctx)
	if err != nil {
		return err
	}

	resp, err := http.Get(*issuerInvitationEndpoint)
	if err != nil {
		return err
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	go func() {
	}()

	invitation := &didexchange.Invitation{}
	err = json.Unmarshal(data, invitation)
	if err != nil {
		return err
	}

	issuerConnectionID, err = client.HandleInvitation(invitation)
	if err != nil {
		return err
	}

	return nil
}

func proposeCredential(ctx *context.Provider) error {
	client, err := issuecredential.New(ctx)
	if err != nil {
		return err
	}

	didEx, err := didexchange.New(ctx)
	if err != nil {
		return err
	}

	connectionRecord, err := didEx.GetConnection(issuerConnectionID)
	if err != nil {
		return err
	}

	_, err = client.SendProposal(&issuecredential.ProposeCredential{}, connectionRecord.Record)
	if err != nil {
		return err
	}

	return nil
}

func proposePresentation(ctx *context.Provider) error {
	client, err := presentproof.New(ctx)
	if err != nil {
		return err
	}

	didEx, err := didexchange.New(ctx)
	if err != nil {
		return err
	}

	connectionRecord, err := didEx.GetConnection(issuerConnectionID)
	if err != nil {
		return err
	}

	_, err = client.SendProposePresentation(&presentproof.ProposePresentation{}, connectionRecord.Record)
	if err != nil {
		return err
	}

	return nil
}

// setUpGlobalParams sets up the parameters used in the app.
func setUpGlobalParams() error {
	level, err := logrus.ParseLevel(*logLevel)
	if err != nil {
		return fmt.Errorf("failed to setup logrus, reason %w", err)
	}
	logrus.SetLevel(level)
	// TODO: don't allow insecure TLS connections on prod
	http.DefaultTransport.(*http.Transport).TLSClientConfig = &tls.Config{InsecureSkipVerify: *allowInsecureHttps}
	if *allowInsecureHttps {
		logrus.Warn("insecure TLS connections are allowed, this shouldn't be the case on prod systems")
	}

	return nil
}

func startAriesAgent() (*context.Provider, error) {
	// TLS certs left empty on purpose
	inboundHttp, err := arieshttp.NewInbound(
		*ariesInternalListenAddress, *ariesExternalListenAddress, "", "",
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create inbound transport method, reason: %w", err)
	}

	framework, err := aries.New(
		aries.WithInboundTransport(inboundHttp),
		aries.WithStoreProvider(
			leveldb.NewProvider("testdb"),
		),
	)
	if err != nil {
		return nil, fmt.Errorf("failed to create framework, reason: %w", err)
	}

	// get the context from the framework
	ctx, err := framework.Context()
	if err != nil {
		return nil, fmt.Errorf("failed to create framework context, reason: %w", err)
	}

	err = AddDidExchange(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to add didExchangeClient to the framework context, reason: %w", err)
	}

	err = AddCreds(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to add issuecredential to the framework context, reason: %w", err)
	}

	err = AddPres(ctx)
	if err != nil {
		return nil, fmt.Errorf("failed to add presentproof to the framework context, reason: %w", err)
	}

	logrus.WithFields(
		logrus.Fields{
			"ariesInternalListenAddress": ariesInternalListenAddress,
			"ariesExternalListenAddress": ariesExternalListenAddress,
		}).Infof("started aries agent")

	return ctx, err
}
