package main

import (
	"fmt"
	"github.com/hyperledger/aries-framework-go/pkg/client/didexchange"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/common/service"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	"github.com/sirupsen/logrus"
)

func AddDidExchange(ctx *context.Provider) error {
	logrus.Infof("adding did exchange support to the agent")

	didExActions := make(chan service.DIDCommAction, 10)
	didExchangeClient, err := didexchange.New(ctx)

	if err != nil {
		return fmt.Errorf("failed to create did exchange client, reason: %w", err)
	}

	err = didExchangeClient.RegisterActionEvent(didExActions)

	if err != nil {
		return fmt.Errorf("failed to register did exchange actions, reason: %w", err)
	}

	// accept all didEx actions
	go func() {
		service.AutoExecuteActionEvent(didExActions)
	}()

	logrus.Infof("successfully added did exchange support to the agent")

	return nil
}
