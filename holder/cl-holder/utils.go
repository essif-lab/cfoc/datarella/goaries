package main

import (
	"bytes"
	"fmt"
	"io"
)

type credxVDR interface {
	Get(key string) ([]byte, error)

	Put(key string, data []byte) error
}

type simpleRestCredxVDR struct {
	httpClient HttpClient
	endpoint   string
}

func (s *simpleRestCredxVDR) Get(key string) ([]byte, error) {
	res, err := s.httpClient.Get(fmt.Sprintf("%s?k=%s", s.endpoint, key))
	if err != nil {
		return nil, fmt.Errorf("failed to resolve key=\"%s\" at %s, reason %w", key, s.endpoint, err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("failed to read body of key=\"%s\" at %s, reason %w", key, s.endpoint, err)
	}
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("received status %d", res.StatusCode)
	}

	return body, nil
}

func (s *simpleRestCredxVDR) Put(key string, data []byte) error {
	res, err := s.httpClient.Post(
		fmt.Sprintf("%s?k=%s", s.endpoint, key),
		"application/json",
		bytes.NewReader(data),
	)
	if err != nil {
		return fmt.Errorf("couldn't post data to %s", key)
	}

	if res.StatusCode != 200 {
		return fmt.Errorf("received status %d", res.StatusCode)
	}

	return nil
}
