package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	client "github.com/hyperledger/aries-framework-go/pkg/client/presentproof"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/common/service"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/decorator"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/presentproof"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	credx "github.com/hyperledger/indy-shared-rs/wrappers/golang/indy-credx/types"
	"github.com/sirupsen/logrus"
	"strconv"
)

func AddPres(ctx *context.Provider) error {
	logrus.Infof("adding credential issuing support to the agent")

	presActions := make(chan service.DIDCommAction, 10)
	presentProofClient, err := client.New(ctx)
	if err != nil {
		return fmt.Errorf("failed to create issue credential client, reason: %w", err)
	}
	err = presentProofClient.RegisterActionEvent(presActions)
	if err != nil {
		return fmt.Errorf("failed to register issue credential actions, reason: %w", err)
	}

	go func() {
		for action := range presActions {
			go handlePresentProofActions(ctx, presentProofClient, action)
		}
	}()

	logrus.Infof("successfully added credential issuing support to the agent")

	return nil
}

func handlePresentProofActions(ctx *context.Provider, presentProofClient *client.Client, action service.DIDCommAction) {
	var err error

	switch action.Message.Type() {
	case presentproof.RequestPresentationMsgTypeV2:
		err = handlePresentationReq(ctx, presentProofClient, action)
	}

	if err != nil {
		logrus.Errorf("failed to handle an issue cred action, %s", err)
	}
}

func handlePresentationReq(ctx *context.Provider, proofClient *client.Client, action service.DIDCommAction) error {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return fmt.Errorf("couldn't open store %s", err)
	}

	piid, ok := action.Properties.All()["piid"].(string)
	if !ok {
		return fmt.Errorf("failed to read piid")
	}

	presReq := &client.RequestPresentationV2{}
	err = action.Message.Decode(presReq)
	if err != nil {
		return fmt.Errorf("failed decode presReq, reason %s", err)
	}

	logrus.WithField(
		"presentationRequest", presReq.RequestPresentationsAttach[0].Data.JSON,
	).Infof("received presentation request")

	credJSON, err := store.Get("credx_cred")
	if err != nil {
		return fmt.Errorf("failed to get credential, reason %s", err)
	}

	cred, err := credx.LoadCredentialFromJSON(credJSON)
	if err != nil {
		return fmt.Errorf("failed to load credential from json reason %s", err)
	}

	presReqJSON, err := json.Marshal(presReq.RequestPresentationsAttach[0].Data.JSON)
	if err != nil {
		return fmt.Errorf("failed to marshall pres req to json, reason %s", err)
	}

	timestamp := presReq.RequestPresentationsAttach[0].Data.JSON.(map[string]interface{})["requested_attributes"].(map[string]interface{})["reft"].(map[string]interface{})["non_revoked"].(map[string]interface{})["from"]

	credxPresReq, err := credx.PresentationRequestFromJSON(presReqJSON)
	if err != nil {
		return fmt.Errorf("failed to load presReq from JSON, reason %s", err)
	}

	masterSecretJSON, err := store.Get("masterSecret")
	if err != nil {
		return fmt.Errorf("failed to read masterSecret from store, reason %s", err)
	}

	masterSecret, err := credx.LoadMasterSecretFromJSON(masterSecretJSON)
	if err != nil {
		return fmt.Errorf("failed to load masterSecret from json, reason %s", err)
	}

	revID, err := cred.GetRevocationRegistryID()
	if err != nil {
		return fmt.Errorf("couldn't read reg rev id, reason %w", err)
	}

	revRegDefJSON, err := AppCtx().credxVDR.Get(revID)
	if err != nil {
		return fmt.Errorf("couldn't read rev with id %s from remote, reason %w", revID, err)
	}

	revRegDef, err := credx.LoadRevocationRegistryDefinitionFromJSON(revRegDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't load revRegDef from json, reason %s", err)
	}

	revRegDeltaJSON, err := AppCtx().credxVDR.Get(revRegDeltaID)
	if err != nil {
		return fmt.Errorf("couldn't read rev delta with id %s from remote, reason %w", revID, err)
	}

	revRegDelta, err := credx.LoadRevocationRegistryDeltaFromJSON(revRegDeltaJSON)
	if err != nil {
		return fmt.Errorf("couldn't load registry delta from json, reason %w", err)
	}

	timestampInt, _ := strconv.ParseInt(fmt.Sprint(timestamp), 10, 64)

	regIdx, err := cred.GetRevocationRegistryIndex()
	if err != nil {
		return fmt.Errorf("couldnt get rev idx, reason %w", err)
	}

	regIdxInt, err := strconv.ParseInt(regIdx, 10, 64)
	if err != nil {
		return fmt.Errorf("couldnt parse rev idx, reason %w", err)
	}

	revState, err := credx.NewCredentialRevocationState(
		revRegDef,
		revRegDelta,
		regIdxInt,
		timestampInt,
	)
	if err != nil {
		return fmt.Errorf("couldn't create revocation state, reason %w", err)
	}

	credEntry := credx.NewCredentialEntry(cred, timestampInt, revState)

	credProve := credx.NewCredentialProve(0, "reft", false, true)

	schemaID, err := cred.GetSchemaID()
	if err != nil {
		return fmt.Errorf("couldn't get schema ID, reason %w", err)
	}

	schemaJSON, err := AppCtx().credxVDR.Get(schemaID)
	if err != nil {
		return fmt.Errorf("couldn't get schema with id=%s from remote store, reason %w", err)
	}

	schema, err := credx.LoadSchemaFromJSON(schemaJSON)
	if err != nil {
		return fmt.Errorf("failed to load creddef json, reason %s", err)
	}

	credDefID, err := cred.GetCredentialDefinitionID()
	if err != nil {
		return fmt.Errorf("couldn't get cred def ID, reason %w", err)
	}

	credDefJSON, err := AppCtx().credxVDR.Get(credDefID)
	if err != nil {
		return fmt.Errorf("couldn't get cred def with id=%s from remote store, reason %w", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON(credDefJSON)
	if err != nil {
		return fmt.Errorf("failed to marshall credential offer, reason %s", err)
	}

	pres, err := credx.NewPresentation(
		credxPresReq,
		[]credx.CredentialEntry{*credEntry},
		[]credx.CredentialProve{*credProve},
		[]string{},
		[]string{},
		masterSecret,
		[]credx.Schema{*schema},
		[]credx.CredentialDefinition{*credDef},
	)
	if err != nil {
		return fmt.Errorf("failed to create presentation, reason %s", err)
	}

	presJSON, err := pres.ToJSON()
	if err != nil {
		return fmt.Errorf("failed to marshal presentation to json, reason %s", err)
	}

	attachID := uuid.New().String()
	err = proofClient.AcceptRequestPresentation(
		piid,
		&presentproof.PresentationParams{
			Attachments: []decorator.GenericAttachment{{
				ID: attachID,
				Data: decorator.AttachmentData{
					JSON: presJSON,
				},
			}},
		},
		nil,
	)
	if err != nil {
		return fmt.Errorf("failed to accept pres req, reason %s", err)
	}

	logrus.WithField(
		"presentation", string(presJSON),
	).Debugf("responding to presentation request")

	return nil
}
