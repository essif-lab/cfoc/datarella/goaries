package main

import (
	"encoding/json"
	"fmt"
	"github.com/google/uuid"
	client "github.com/hyperledger/aries-framework-go/pkg/client/issuecredential"
	"github.com/hyperledger/aries-framework-go/pkg/client/issuecredential/rfc0593"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/common/service"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/decorator"
	"github.com/hyperledger/aries-framework-go/pkg/didcomm/protocol/issuecredential"
	"github.com/hyperledger/aries-framework-go/pkg/framework/context"
	credx "github.com/hyperledger/indy-shared-rs/wrappers/golang/indy-credx/types"
	"github.com/sirupsen/logrus"
)

const (
	credDefID     = "credDef1"
	revRegDeltaID = "revRegDelta1"
)

func AddCreds(ctx *context.Provider) error {
	logrus.Infof("adding credential issuing support to the agent")

	issueCredActions := make(chan service.DIDCommAction, 10)
	issueCredentialSvc, err := client.New(ctx)
	if err != nil {
		return fmt.Errorf("failed to create issue credential client, reason: %w", err)
	}

	err = issueCredentialSvc.RegisterActionEvent(issueCredActions)
	if err != nil {
		return fmt.Errorf("failed to register issue credential actions, reason: %w", err)
	}

	go func() {
		for action := range issueCredActions {
			go handleIssueCredActions(ctx, issueCredentialSvc, action)
		}
	}()

	logrus.Infof("successfully added credential issuing support to the agent")

	return nil
}

func handleIssueCredActions(ctx *context.Provider, service *client.Client, action service.DIDCommAction) {
	var err error

	switch action.Message.Type() {
	case issuecredential.OfferCredentialMsgTypeV2:
		err = handleCredentialOffer(ctx, service, action)
	case issuecredential.IssueCredentialMsgTypeV2:
		err = handleIssueCredential(ctx, action)
	}

	if err != nil {
		logrus.Errorf("failed to handle an issue cred action, %s", err)
	}
}

func handleCredentialOffer(ctx *context.Provider, service *client.Client, action service.DIDCommAction) error {
	offer := &client.OfferCredentialV2{}
	err := action.Message.Decode(offer)
	if err != nil {
		return fmt.Errorf("couldn't decode the proposal, reason %w", err)
	}

	credOfferJSON, err := json.Marshal(offer.OffersAttach[0].Data.JSON)
	if err != nil {
		return fmt.Errorf("couldn't marshall the cred offer, reason %w", err)
	}

	credxOffer, err := credx.LoadCredentialOfferFromJSON(credOfferJSON)
	if err != nil {
		return fmt.Errorf("couldn't load credx offer, reason %w", err)
	}

	piid, ok := action.Properties.All()["piid"].(string)

	logrus.WithField("piid", piid).Infof("handling credential proposal")
	logrus.WithFields(logrus.Fields{
		"piid":  piid,
		"offer": offer,
	}).Debugf("received offer")

	if !ok {
		return fmt.Errorf("couldn't get piid from propsal")
	}

	credReq, err := getCredRequest(ctx, credxOffer)
	if err != nil {
		return err
	}

	err = service.AcceptOffer(piid, credReq)
	if err != nil {
		return err
	}

	credReqJSON, err := json.Marshal(credReq.Attachments[0].Data.JSON)
	if err != nil {
		return fmt.Errorf("couldn't marshall credReq to json, reason %w", err)
	}

	logrus.WithFields(logrus.Fields{
		"piid":    piid,
		"credReq": string(credReqJSON),
	}).Debugf("credential offer was accepted, responding with credential request")

	return nil
}

func getCredRequest(ctx *context.Provider, credOffer *credx.CredentialOffer) (*issuecredential.RequestCredentialParams, error) {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return nil, fmt.Errorf("couldn't open store %s", err)
	}

	credDefJSON, err := AppCtx().credxVDR.Get(credDefID)
	if err != nil {
		return nil, fmt.Errorf("couldn't get cred def with id=%s from remote store, reason %w", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON(credDefJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credential offer, reason %s", err)
	}

	ms, err := credx.NewMasterSecret()
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credential offer, reason %s", err)
	}

	masterSecret, err := ms.ToJSON()
	if err != nil {
		return nil, fmt.Errorf("couldn't marshall master secret")
	}

	err = store.Put("masterSecret", masterSecret)
	if err != nil {
		return nil, fmt.Errorf("couldn't store master secret, reason %s", err)
	}

	credReq, credReqMetadata, err := credx.NewCredentialRequest(
		"did:prover:123",
		credDef,
		ms,
		"test_123",
		credOffer,
	)
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credential offer, reason %s", err)
	}

	credReqJSON, err := credReq.ToJSON()
	if err != nil {
		return nil, err
	}

	credReqMetaJSON, err := credReqMetadata.ToJSON()
	if err != nil {
		return nil, fmt.Errorf("failed to marshall credReqMetadata, reason %s", err)
	}

	err = store.Put("cred-req-metadata", credReqMetaJSON)
	if err != nil {
		return nil, fmt.Errorf("failed to store credReqMetadata, reason %s", err)
	}

	attachID := uuid.New().String()
	return &client.RequestCredential{
		Type: issuecredential.RequestCredentialMsgTypeV2,
		Formats: []issuecredential.Format{{
			AttachID: attachID,
			Format:   rfc0593.ProofVCFormat,
		}},
		Attachments: []decorator.GenericAttachment{{
			ID: attachID,
			Data: decorator.AttachmentData{
				JSON: credReqJSON,
			},
		}},
	}, nil
}

func handleIssueCredential(ctx *context.Provider, action service.DIDCommAction) error {
	store, err := ctx.StorageProvider().OpenStore(credxStoreName)
	if err != nil {
		return fmt.Errorf("couldn't open store %s", err)
	}

	issueCred := &client.IssueCredentialV2{}
	err = action.Message.Decode(issueCred)
	if err != nil {
		return fmt.Errorf("couldn't decode issueCred, reason %s", err)
	}

	logrus.WithField(
		"credential", issueCred.CredentialsAttach[0].Data.JSON,
	).Infof("received credential")

	credJSON, err := json.Marshal(issueCred.CredentialsAttach[0].Data.JSON.(map[string]interface{})["credentialSubject"])
	if err != nil {
		return fmt.Errorf("couldn't marshall cred to json")
	}

	cred, err := credx.LoadCredentialFromJSON(credJSON)
	if err != nil {
		return fmt.Errorf("failed to load credential from json reason %s", err)
	}

	credDefID, err := cred.GetCredentialDefinitionID()
	if err != nil {
		return fmt.Errorf("couldn't get cred def ID, reason %w", err)
	}

	credDefJSON, err := AppCtx().credxVDR.Get(credDefID)
	if err != nil {
		return fmt.Errorf("couldn't get cred def with id=%s from remote store, reason %w", err)
	}

	credDef, err := credx.LoadCredentialDefinitionFromJSON([]byte(credDefJSON))
	if err != nil {
		return fmt.Errorf("couldn't load credDef from json")
	}

	masterSecretJSON, err := store.Get("masterSecret")
	if err != nil {
		return fmt.Errorf("failed to load master secret from store, reason %s", err)
	}

	masterSecret, err := credx.LoadMasterSecretFromJSON(masterSecretJSON)
	if err != nil {
		return fmt.Errorf("failed to load master secret from json, reason %s", err)
	}

	credReqMetaJSON, err := store.Get("cred-req-metadata")
	if err != nil {
		return fmt.Errorf("couldnt read credReqMeta from store, reason %s", err)
	}

	credReqMeta, err := credx.LoadCredentialRequestMetadataFromJSON(credReqMetaJSON)
	if err != nil {
		return fmt.Errorf("couldn't load credentialRequestMetadata from json, reason %s", err)
	}

	revID, err := cred.GetRevocationRegistryID()
	if err != nil {
		return fmt.Errorf("couldn't read reg rev id, reason %w", err)
	}

	revRegDefJSON, err := AppCtx().credxVDR.Get(revID)
	if err != nil {
		return fmt.Errorf("couldn't read rev with id %s from remote, reason %w", revID, err)
	}

	revRegDef, err := credx.LoadRevocationRegistryDefinitionFromJSON(revRegDefJSON)
	if err != nil {
		return fmt.Errorf("couldn't load revRegDef from json, reason %s", err)
	}

	processedCred, err := cred.Process(credReqMeta, masterSecret, credDef, revRegDef)
	if err != nil {
		return fmt.Errorf("couldn't process credential, reason %s", err)
	}

	processedCredJSON, err := processedCred.ToJSON()
	if err != nil {
		return fmt.Errorf("couldn't marshall processed credential, reason %s", err)
	}

	err = store.Put("credx_cred", processedCredJSON)
	if err != nil {
		return fmt.Errorf("couldn't store vc")
	}

	err = store.Flush()
	if err != nil {
		return fmt.Errorf("failed to flush store, reason %s", err)
	}

	logrus.Infof("successfully stored credential")
	event <- true
	return nil
}
