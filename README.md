# essif-credx

To run the demo:
```bash
docker-compose up --build
```

## Sequence Diagram
![](./Aries_Bridge_Demo.png)
