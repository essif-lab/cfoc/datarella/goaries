package main

import (
	"encoding/json"
	"github.com/kataras/iris/v12"
	"log"
)

const (
	listenAddress = "0.0.0.0:9999"
)

var storage = make(map[string]json.RawMessage)

func main() {
	err := startControllerHttpServer()
	if err != nil {
		log.Fatal(err)
	}
}

func startControllerHttpServer() error {
	app := iris.New()

	app.Get("/health", func(ctx iris.Context) {
		ctx.StatusCode(200)
	})
	app.Get("/", func(ctx iris.Context) {
		key := ctx.Request().URL.Query().Get("k")
		data := storage[key]
		_, err := ctx.JSON(data)
		if err != nil {
			ctx.StatusCode(500)
			return
		}
		ctx.ContentType("application/json")
		ctx.StatusCode(200)
	})
	app.Post("/", func(ctx iris.Context) {
		key := ctx.Request().URL.Query().Get("k")
		data, err := ctx.GetBody()
		if err != nil {
			ctx.StatusCode(500)
			return
		}

		storage[key] = data
		ctx.StatusCode(200)
	})

	err := app.Listen(listenAddress)

	return err
}
